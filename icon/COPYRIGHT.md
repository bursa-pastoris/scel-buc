`scel-buc`'s icon is an adapted work from the following works:

- [Meuble héraldique Cerf
  courant](https://commons.wikimedia.org/wiki/File:Meuble_h%C3%A9raldique_Cerf_courant.svg)
  from Syryatsu, distributed under the following licenses:
  - GFDL-1.2-or-later
  - CC BY-SA 3.0 Unported
  - CC BY-SA 2.5 Generic
  - CC BY-SA 2.0 Generic
  - CC BY-SA 1.0 Generic
- [Beaker font
  awesome](https://commons.wikimedia.org/wiki/File:Beaker_font_awesome.svg)
  from Dave Gandy, distributed under the following licenses:
  - CC BY-SA 3.0 Unported
