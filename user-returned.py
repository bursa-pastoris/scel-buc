#!/bin/env python3

# scel-buc - scripts to send and receive messages through a Telegram bot
# Copyright (C) 2023 bursa-pastoris
#
# This file is part of scel-buc.
#
# scel-buc is free software: you can redistribute it and/or modify it under the
# terms of the GNU Affero General Public License as published by the Free
# Software Foundation, version 3 of the License.
#
# scel-buc is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with scel-buc. If not, see <https://www.gnu.org/licenses/>.

from telegram import Update
from telegram.ext import ApplicationBuilder, MessageHandler, filters

from constants import *


async def user_back(update, context):
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        reply_to_message_id=update.effective_message.id,
        text=REDIRECT_MSG)
    if SEND_SENDER:
        sender = USER_NAMES[str(update.effective_chat.id)]
        await context.bot.send_message(
            chat_id=OWNER_ID,
            text=f'*Message from {sender}:*',
            parse_mode='MarkdownV2')
    await context.bot.forward_message(
        chat_id=OWNER_ID,
        from_chat_id=update.effective_chat.id,
        message_id=update.effective_message.id)


application = ApplicationBuilder().token(TOKEN).build()

application.add_handler(
    MessageHandler((filters.User([int(i) for i in USER_NAMES.keys()]) & filters.ChatType.PRIVATE),
                   user_back)
    )

application.run_polling()
