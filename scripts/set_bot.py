#!/bin/env python3

# scel-buc - scripts to send and receive messages through a Telegram bot
# Copyright (C) 2023 bursa-pastoris
#
# This file is part of scel-buc.
#
# scel-buc is free software: you can redistribute it and/or modify it under the
# terms of the GNU Affero General Public License as published by the Free
# Software Foundation, version 3 of the License.
#
# scel-buc is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with scel-buc. If not, see <https://www.gnu.org/licenses/>.

import urllib.request, urllib.parse
import json
from time import sleep

from constants import *

bot_name = urllib.parse.quote(f"{USER}'s scel-buc instance")
bot_desc = urllib.parse.quote(
    f"Writing to this bot, you can contact {USER} while he/she can't access "
    f"his/her account."
    f" Just start this bot and introduce yourself (otherwise {USER} won't be"
    f" able to contact you nor to know who you are)."
    f" You will not receive an automatic answer, but that's expected: {USER}"
    f" will have to answer manually.\n"
    f"\n"
    f"This bot is powerd by scel-buc v{VERSION}, a free (as in freedom, not as"
    f" in free beer) software distributed under the AGPL-3.0-only license."
    f" Its source code is available at"
    f" https://git.disroot.org/bursa-pastoris/scel-buc.")
bot_shortdesc = urllib.parse.quote(
    f"{USER}'s scel-buc instance")

urllib.request.urlopen(f'https://api.telegram.org/bot{TOKEN}/setMyName?name={bot_name}')
print('Bot name set')
sleep(1)
urllib.request.urlopen(f'https://api.telegram.org/bot{TOKEN}/setMyDescription?description={bot_desc}')
print('Bot description set')
sleep(1)
urllib.request.urlopen(f'https://api.telegram.org/bot{TOKEN}/setMyShortDescription?short_description={bot_shortdesc}')
print('Bot short description set')
print('To set a bot picture you have to use the BotFather, as there is no way'
      ' to do it throught the API')
