# This file is part of scel-buc.
#
# scel-buc is free software: you can redistribute it and/or modify it under the
# terms of the GNU Affero General Public License as published by the Free
# Software Foundation, version 3 of the License.
#
# scel-buc is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Foobar. If not, see <https://www.gnu.org/licenses/>.

import configparser
from os import makedirs
from os.path import exists as path_exists

config = configparser.ConfigParser()
config.read('settings.ini')

# bot
TOKEN = config['bot']['token']
USER = config['bot']['user']

# settings
PROXY = config['settings']['proxy']
CHAT_PATH = config['settings']['chat_path']
makedirs(CHAT_PATH+'/data', exist_ok=True)

# user returned
OWNER_ID = config['user_returned']['owner_id']
SEND_SENDER = config['user_returned'].getboolean('send_sender')
REDIRECT_MSG = config['user_returned']['redirect_msg']

# internal
if not path_exists('.last_update'):
    with open('.last_update','w') as f:
        f.write('0')
with open('.last_update','r') as f:
    LAST_UPDATE = int(f.read())

API = f'https://api.telegram.org/bot{TOKEN}'

MESSAGES = CHAT_PATH+'messages.json'

USER_NAMES = {i:config['users'][i] for i in config['users']}
GROUP_NAMES = {i:config['groups'][i] for i in config['groups']}
VERSION = '0.1.0'
