---
gitea: none
include_toc: true
---

`scel-buc` is a couple of tool to use a Telegram bot to send and receive
messages as if it was a regular user account. It was being developed because I
lost access to my Telegram account, so it followed the "just make it work"
philosophy. It was slightly refined and it will hoepfully be more in future.

`scel-buc` is distributed under the [AGPL-3.0-only license](./LICENSE).

`scel-buc` means _`sc`ripts for T`el`egram `b`ot `u`ser `c`lient_. I don't know
if the stuff can still be considered a set of scripts but I can't come up with
another stupid acronym anyway.

## Features and limitations

`scel-buc.py` is a CLI tool to send and receive messages. It is the main piece
of the repository.

`scel-buc.py` can only send and receive unformatted text messages from single
and group chats:

- There is no way to send anything other than simple text messages.
- received messages of unsupported
  [types](https://core.telegram.org/bots/api#available-types) (e.g.
  [audio](https://core.telegram.org/bots/api#audio) or [animated
  dice](https://core.telegram.org/bots/api#audio) will be logged as
  `[unsupported message type]`.
- Formatting, _including inline links_, is stripped from received message
  _without notice_. For example, if someone sends the message

  > _Nobody_ expects [the Spanish Inquisition](https://upload.wikimedia.org/wikipedia/commons/a/af/Monty_Python_Live_02-07-14_12_47_50_%2814415365418%29.jpg)!

  it will be _silently_ logged as

  > Nobody expects the Spanish Inquisition!

Any contribution adding support for unsupported message types is welcome.

If an unsupported message is received, its content can be retrieved by
accessing [Telegram's bot API](https://core.telegram.org/bots/api#getupdates)
manually, _before `scel-buc` is used another time. After that, the message is
gone._

## Caveats

1. `scel-buc` sends and receive Telegram messages. Since it works through a
   Telegram bot, it must receive a message from the user before being able to
   send one to him.
2. Telegram keeps unfetched messages to bots [for 24 hours
   only](https://core.telegram.org/bots/api#getting-updates); therefore, you
   should run fetch them at least once every 24 hours, otherwise you _will_
   lose messages.

## How to
### Basics

Messages are stored in the path defined in the [settings](#settings) and
organized in HTML files. Open `index.html` in any browser and navigate to
single chats.

CSS is very poor. Any contribution improving it is welcome.

`scel-buc` is CLI tool with very clear commands and arguments. Here's the CLI
help.

```
$ scel-buc -h
usage: scel-buc [-h] {get-updates,build-chats,send-message} ...

options:
  -h, --help            show this help message and exit

Subcommands:
  {get-updates,build-chats,send-message}
    get-updates         receive new updates
    build-chats         write the chats to HTML
    send-message        send a message
```

```
$ scel-buc.py get-updates -h
usage: scel-buc get-updates [-h]

options:
  -h, --help  show this help message and exit
```

```
$ scel-buc.py build-chats -h
usage: scel-buc build-chats [-h]

options:
  -h, --help  show this help message and exit
```

```
$ ./scel-buc.py send-message -h
usage: scel-buc send-message [-h] --id ID [--text TEXT]

options:
  -h, --help   show this help message and exit
  --id ID      ID of the chat
  --text TEXT  message text. If omitted, the default text editor is opened to
               compose the message.
```

### Set up

`scel-buc` must be run through a Telegram bot account, that can be created by
contacting the [BotFather](https://t.me/BotFather). If you plan to use the bot
in groups, you must _disable_ the [privacy
mode](https://core.telegram.org/bots/features#privacy-mode) before adding it.
If you forget to do so, you can disable privacy mode, remove the bot from the
group and add it again.

Since `scel-buc` is meant to be used when you lost access to your regular
Telegram account, you must have created a bot account beforehand and stored its
token. If you didn't, you can ask someone _trusted_ to create one for you.
Remember that whoever creates the bot account can, and is the only one who can,
retrieve and change its token at any time, so he can hijack the bot.

Also, if your account
[auto-deletes](https://telegram.org/privacy#10-4-account-self-destruction) all
your bots are lost.

### Settings

`scel-buc` is configured by filling the options in
[`settings.ini`](./settings.ini). Here's the explanation of what they mean.

- `bot` section. All settings in this section are mandatory.
  - `token`: the token of the bot
  - `user`: the name of the user who will use the bot. This can be any string.
- `settings` section. All settings in this section are mandatory.
  - `chat_path`: the absolute path where the chats will be stored. The path
    must exist and end with the `/` char. Path expansion is _not_ supported.
- `users` section.

  By default, `scel_buc` will indicate any user who contacts you with his
  Telegram user ID.[^why-user-id] In this section you may add a `user_id:
  user_name` entry for any user you communicate with and such a `user_name`
  will be used instead of the `user_id`.

  An example entry can be the following:
  ```
  123456789: John Doe
  ```
- `groups` section.

  Same as `users` section but for groups.[^group-id-dash] If no `group_name` is
  given, the name of the group in Telegram will be used. This section is
  intended to allow you to prevent file names with emojis or other weirdnesses.

  An example entry can be the following:
  ```
  987654321: The Spanish Inquisition
  ```
- `user_returned` section. If you use [`user-returned.py`](#user-returned) all
  settings in this section are mandatory.

  This section is meant to inform your contacts who got used to converse with
  you through the bot that you are back and they should go back to your normal
  account.

  - `owner_id`: your Telegram user ID.
  - `send_sender`: whether the bot must send you a message specifing the
    original sender before forwarding a mesage to you. Must be a boolean value.
  - `redirect_msg`: the message to send to people contacting the bot. Line
    break is supported by indenting the following lines, formatting is _not_.

## `user-returned.py`

This is a very simple bot to use when you are back on Telegram with a regular
account. It:

1. automatically answer _each_ _private_ message redirecting the user to your
   regular account;
2. forwards all _private_ messages to your regular account

It requires `pyhon-telegram-bot`, you can install the correct version from
[`requirements.txt`](./requirements.txt)

To answer immediately to the messages, the bot must keep running.


[^why-user-id]: This is because in Telegram picking is not mandatory to pick a
username, as well as indicating a first name or a last name (you can give the
first, _or_ the last, _or_ both). Therefore, it would be complex to generate a
unique identifier based on these values.
[^group-id-dash]: The group ID may include a `-`: in such a case, the dash
_must_ be specified.
